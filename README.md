A coding exam I did for <Company> last year.

The program parses variables and equations and evaluates them.

Example: let(a, let(b, 10, add(b, b)), let(b, 20, add(a, b)))